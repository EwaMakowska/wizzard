export interface Employee {
    name: string;
    surname: string;
    pin: number | null;
  }
