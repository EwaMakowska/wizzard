export interface Company {
    name: string;
    surname: string;
    employees: number | null;
    phone: number | null;
    brand: string;
  }
